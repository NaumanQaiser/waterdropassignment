# README #

### What is this repository for? ###

* Waterdrop Home assignment

Task Details
 
* Screen 1: 
* Allows to consume an api endpoint of your choice. The screen should have some form of input 
* component (can be a picker, textfield or something else) in order to be able to modify the api 
* request and a button to execute the api request.
* Example: myapi.com/itemlist?catergory=X
* Loaded data should then be displayed in a list on this screen (you can choose how much 
* information you show of each item). Selecting a list item should navigate to screen 2.
* Screen_2: 
* Displays selected items data and additionally allows to take a photo. Once a photo is taken it 
* should be displayed on this screen as well. Lastly this screen offers the possibility to store the 
* item data + the image on the device.

* The ui is not the essential part of this assignment, you can make it look pretty though
* If you make any assumptions, please write them down somewhere
* If you don’t know an api, we would suggest using the free api from openewheatermap.org 
* Please provide the full source code in a zip file or host it on GitHub and provide the ur

### Project Flow  Architecture MVVM - Retrofit - Glide - Robolectric - Room - Hilt - Dependency Injection ###

* Developed app using free Newsorg.api following MVVM Architecture
* User can subscribe to any keywords to get the relevant news List from the drop down on the home fragment.
* In Details fragment user can take a screeshot of the article. Application also allows user to save the screenshot of the article in internal/External storage in Pictures/Waterdrop_screenshots.
* Used Room database to save articles in the presistent storage.

* News Api :The main use of News API is to search through every article published by over 80,000 news sources and blogs in the last 3 years. Think of us as Google News that you can interact with programmatically! https://newsapi.org/


### Assumptions ###

"Additionally allows to take a photo"

* App would take a screenshort/photo of the view on the second screen and save it to External/internal storage.
* I used the room database to save item data and image Url.

### Todo's ###
* Writes tests for storing Image File
* Tests for database funtions
* Tests for spinner 

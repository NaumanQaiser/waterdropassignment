package com.one.waterdropchallenge.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.one.waterdropchallenge.R;
import com.one.waterdropchallenge.models.NewsArticle;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class NewsArticlesAdapter extends RecyclerView.Adapter<NewsArticlesAdapter.ViewHolder> {

    Context context;
    List<NewsArticle> articleArrayList = new ArrayList<>();
    private final OnRecyclerViewItemClickListener itemClickListener;

    @Inject
    public NewsArticlesAdapter(Context context, OnRecyclerViewItemClickListener listener) {
        this.context = context;
        this.itemClickListener = listener;
    }

    @NonNull
    @Override
    public NewsArticlesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_item, viewGroup, false);
        return new ViewHolder(view);
    }

    public void setResults(List<NewsArticle> articles) {
        this.articleArrayList = articles;
    }

    @Override
    public void onBindViewHolder(@NonNull NewsArticlesAdapter.ViewHolder viewHolder, int i) {
        NewsArticle article = articleArrayList.get(i);
        viewHolder.tvTitle.setText(article.getTitle());
        viewHolder.tvAuthorAndPublishedAt.setText("-" + article.getAuthor() + " | " + "Published At: " + article.getPublishedAt());
        viewHolder.tvDescription.setText(article.getDescription());
        Glide.with(context)
                .load(article.getUrlToImage())
                .placeholder(R.drawable.progress_anim)
                .into(viewHolder.imgViewCover);

        viewHolder.main.setOnClickListener(v -> itemClickListener.onClick(article.getTitle(), article.getDescription(), article.getUrlToImage()));
    }

    @Override
    public int getItemCount() {
        return articleArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView imgViewCover;
        public final TextView tvTitle;
        private final TextView tvAuthorAndPublishedAt;
        private final TextView tvDescription;
        RelativeLayout main;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            main = itemView.findViewById(R.id.main);
            imgViewCover = itemView.findViewById(R.id.imgViewCover);
            tvTitle =  itemView.findViewById(R.id.tvTitle);
            tvAuthorAndPublishedAt =  itemView.findViewById(R.id.tvAuthorAndPublishedAt);
            tvDescription = itemView.findViewById(R.id.tvDescription);
        }
    }

    public interface OnRecyclerViewItemClickListener {
        void onClick(String title, String des, String imgUrl);
    }
}
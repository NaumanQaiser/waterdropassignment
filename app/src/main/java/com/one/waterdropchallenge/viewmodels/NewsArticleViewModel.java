package com.one.waterdropchallenge.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.one.waterdropchallenge.apiresponse.ApiResponse;
import com.one.waterdropchallenge.repositories.NewsArticleRepository;

import javax.inject.Inject;

import static com.one.waterdropchallenge.constants.constants.API_KEY;
import static com.one.waterdropchallenge.constants.constants.ARTICLE_QUERY;

public class NewsArticleViewModel extends ViewModel {

    private LiveData<ApiResponse> newsArticles;

    @Inject
    public NewsArticleViewModel() {
        NewsArticleRepository.getInstance().searchArticles(ARTICLE_QUERY, API_KEY);
        newsArticles = NewsArticleRepository.getInstance().getArticles();
    }

    public void update() {
        NewsArticleRepository.getInstance().searchArticles(ARTICLE_QUERY, API_KEY);
    }

    public LiveData<ApiResponse> getNewsArticles() {
        return newsArticles;
    }

}


package com.one.waterdropchallenge.repositories;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.one.waterdropchallenge.apiresponse.ApiResponse;
import com.one.waterdropchallenge.webservice.ApiRequest;
import com.one.waterdropchallenge.webservice.RetrofitRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NewsArticleRepository {
    private String TAG = NewsArticleRepository.class.getSimpleName();
    private ApiRequest apiRequest;
    private MutableLiveData<ApiResponse> data;

    public static NewsArticleRepository instance;

    public static NewsArticleRepository getInstance() {
        if (instance == null) {
            instance = new NewsArticleRepository();
        }
        return instance;
    }

    private NewsArticleRepository() {
        data = new MutableLiveData<>();
        apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);
    }

    public LiveData<ApiResponse> getArticles() {
        return data;
    }

    public void searchArticles(String query, String key) {
        apiRequest.getMovieArticles(query, key)
                .enqueue(new Callback<ApiResponse>() {

                    @Override
                    public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                        Log.d(TAG, "onResponse response:: " + response);


                        if (response.body() != null) {
                            data.postValue(response.body());

                            Log.d(TAG, "articles total result:: " + response.body().getTotalResults());
                            Log.d(TAG, "articles size:: " + response.body().getArticles().size());
                            Log.d(TAG, "articles title pos 0:: " + response.body().getArticles().get(0).getTitle());
                        }
                    }

                    @Override
                    public void onFailure(Call<ApiResponse> call, Throwable t) {
                        data.postValue(null);
                    }
                });
    }
}

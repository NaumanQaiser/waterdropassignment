package com.one.waterdropchallenge.constants;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import static com.one.waterdropchallenge.WaterdropApplication.getAppContext;

public class constants {
    /**
     * get a api key from https://newsapi.org/
     * just register an account and request for an api key
     * when you will get an api key please replace with YOUR_API_KEY
     */
    public static final String BASE_URL = "https://newsapi.org/";
    public static final String API_KEY = //"8307847289224ee38c0ef5eaab33f36e";
    "e02d87cbc1ca423aa7c69cb663427225";

    static SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getAppContext());
    static String type = preferences.getString("type", "news");

    public static String ARTICLE_QUERY = type;
}

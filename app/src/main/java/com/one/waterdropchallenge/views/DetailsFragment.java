package com.one.waterdropchallenge.views;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.one.waterdropchallenge.R;
import com.one.waterdropchallenge.Room.Articles;
import com.one.waterdropchallenge.Room.ArticlesDataBase;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static android.content.ContentValues.TAG;
import static com.one.waterdropchallenge.WaterdropApplication.getAppContext;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class DetailsFragment extends Fragment {

    ArticlesDataBase dataBase = ArticlesDataBase.getInstance(getAppContext());

    public static DetailsFragment getInstance() {
        return new DetailsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_details, container, false);

        assert this.getArguments() != null;
        String title = this.getArguments().getString("title");
        String des = this.getArguments().getString("des");
        String imageUrl = this.getArguments().getString("imageUrl");

        TextView titletext = root.findViewById(R.id.tvTitle);
        TextView destext = root.findViewById(R.id.tvDescription);
        ImageView im = root.findViewById(R.id.imgViewCover);
        Button saveData = root.findViewById(R.id.save_data);

        saveData.setOnClickListener(view -> {
            Articles article = new Articles(des, title, imageUrl);
            // create worker thread to insert data into database
            try {
                insert(article);
                Toast.makeText(requireActivity(),"Article Data saved to Local Database..!",Toast.LENGTH_SHORT).show();
            }
            catch (Exception e){}

        });
        takeScreenShot(root);

        titletext.setText(title);
        destext.setText(des);
        Glide.with(requireActivity())
                .load(imageUrl)
                .placeholder(R.drawable.progress_anim)
                .into(im);


        return root;
    }

    public void saveBitmap(Bitmap bitmap) {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            //Background work
            String root;
            //root = Environment.getExternalStorageDirectory().toString();
            root = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES).toString();
            File myDir = new File(root + "/Waterdrop_Screenshots");
            myDir.mkdirs();
            Random generator = new Random();
            int n = 10000;
            n = generator.nextInt(n);
            String fname = "Image-" + n + ".jpg";
            File file = new File(myDir, fname);
            Log.i(TAG, "" + file);
            if (file.exists())
                file.delete();
            try {
                FileOutputStream out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    void RetrieveData()
    {
        dataBase.getArticleDao().getAll();
    }

    void insert(Articles article){
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            //Background work
            dataBase.getArticleDao().insert(article);
        });
    }

    void takeScreenShot(View root){
        Button takescreenshot = root.findViewById(R.id.take_image);
        takescreenshot.setOnClickListener(view -> {

            View v1 = root.findViewById(R.id.main_layout); //this works too for particular view
            // but gives only content
            Bitmap myBitmap;
            v1.setDrawingCacheEnabled(true);
            myBitmap = v1.getDrawingCache();

            View view1 = View.inflate(requireActivity(), R.layout.dialog_layout, null);
            ImageView imgRefInflated = view1.findViewById(R.id.imageView);

            imgRefInflated.setImageBitmap(myBitmap);

            AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
            builder.setNegativeButton("Close", (dialog, which) -> dialog.dismiss());

            builder.setPositiveButton("Save Screenshot", (dialog, which) -> {

                saveBitmap(myBitmap);
                dialog.dismiss();
                Toast.makeText(requireActivity(),"Article's Screenshot is successfully saved!",Toast.LENGTH_SHORT).show();
            });

            AlertDialog dialog = builder.create();
            dialog.setView(view1);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.show();
        });
    }

}
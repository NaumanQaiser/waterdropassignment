package com.one.waterdropchallenge.views;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chivorn.smartmaterialspinner.SmartMaterialSpinner;
import com.one.waterdropchallenge.R;
import com.one.waterdropchallenge.adapters.NewsArticlesAdapter;
import com.one.waterdropchallenge.constants.constants;
import com.one.waterdropchallenge.viewmodels.NewsArticleViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */

public class HomeFragment extends Fragment implements NewsArticlesAdapter.OnRecyclerViewItemClickListener {

    RecyclerView rvNewsArticle;
    ProgressBar updateProgress;
    LinearLayoutManager layoutManager;
    @Inject
    NewsArticleViewModel newsArticleViewModel;
    NewsArticlesAdapter newsArticleAdapter;

    private SmartMaterialSpinner<String> spNews;
    private List<String> newsList;

    public static HomeFragment getInstance() {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        rvNewsArticle = root.findViewById(R.id.rv_news_articles);
        updateProgress = root.findViewById(R.id.progress);
        updateProgress.setVisibility(View.VISIBLE);
        spNews = root.findViewById(R.id.spinner1);

        initSpinner();

        newsArticleViewModel = new ViewModelProvider(this).get(NewsArticleViewModel.class);

        newsArticleViewModel.getNewsArticles().observe(getViewLifecycleOwner(), apiResponse -> {
            try {
                updateProgress.setVisibility(View.INVISIBLE);
                newsArticleAdapter.setResults(apiResponse.getArticles());
                newsArticleAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }

        });

        initRecyclerView();

        return root;
    }

    private void initRecyclerView() {
        layoutManager = new LinearLayoutManager(requireActivity());
        rvNewsArticle.setLayoutManager(layoutManager);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        rvNewsArticle.setHasFixedSize(true);
        newsArticleAdapter = new NewsArticlesAdapter(requireActivity(),this);
        rvNewsArticle.setAdapter(newsArticleAdapter);

    }

    @Override
    public void onClick(String title, String description, String imageUrl) {
        DetailsFragment nextFrag = new DetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("des", description);
        bundle.putString("imageUrl", imageUrl);
        nextFrag.setArguments(bundle);

        requireActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentHolder, nextFrag, "findThisFragment")
                .addToBackStack(null)
                .commit();
    }

    private void initSpinner() {

        newsList = new ArrayList<>();

        newsList.add("Crypto");
        newsList.add("football");
        newsList.add("cricket");
        newsList.add("news");
        newsList.add("android");
        newsList.add("politics");
        newsList.add("water");

        spNews.setItem(newsList);

        spNews.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                Toast.makeText(requireActivity(), newsList.get(position), Toast.LENGTH_SHORT).show();
                String newsType = newsList.get(position);
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(requireActivity());
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("type", newsType);
                editor.apply();
                constants.ARTICLE_QUERY = newsType;
                newsArticleViewModel.update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
package com.one.waterdropchallenge;

import android.app.Application;
import android.content.Context;

import com.bumptech.glide.annotation.GlideModule;

import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
public class WaterdropApplication extends Application {
    private static Context context;

    public void onCreate() {
        super.onCreate();
        WaterdropApplication.context = getApplicationContext();
    }
    public static Context getAppContext() {
        return WaterdropApplication.context;
    }
}

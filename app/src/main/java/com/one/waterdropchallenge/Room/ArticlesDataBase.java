package com.one.waterdropchallenge.Room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Articles.class}, version = 1)
public abstract class ArticlesDataBase extends RoomDatabase {

    public abstract ArticleDao getArticleDao();

    private static ArticlesDataBase ArticleDB;

    public static ArticlesDataBase getInstance(Context context) {
        if (null == ArticleDB) {
            ArticleDB = buildDatabaseInstance(context);
        }
        return ArticleDB;
    }

    private static ArticlesDataBase buildDatabaseInstance(Context context) {
        return Room.databaseBuilder(context,
                ArticlesDataBase.class,
                "ArticlesDatabase")
                .allowMainThreadQueries().build();
    }

}

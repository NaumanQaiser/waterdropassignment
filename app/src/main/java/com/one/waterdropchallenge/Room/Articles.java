package com.one.waterdropchallenge.Room;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity
public class Articles {

    @PrimaryKey(autoGenerate = true)
    private int article_id;

    @ColumnInfo(name = "article_content")
    // column name will be "article_content" instead of "content" in table
    private String content;

    private String title;

    private String imageUrl;

    public Articles(String content, String title, String imageUrl) {
        this.content = content;
        this.title = title;
        this.imageUrl = imageUrl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getArticle_id() {
        return article_id;
    }

    public void setArticle_id(int article_id) {
        this.article_id = article_id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Articles)) return false;

        Articles article = (Articles) o;

        if (article_id != article.article_id) return false;
        return Objects.equals(title, article.title);
    }


    @Override
    public int hashCode() {
        int result = article_id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "article{" +
                "article_id=" + article_id +
                ", content='" + content + '\'' +
                ", title='" + title + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }

}

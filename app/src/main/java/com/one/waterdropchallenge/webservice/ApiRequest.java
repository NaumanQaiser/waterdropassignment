package com.one.waterdropchallenge.webservice;

import com.one.waterdropchallenge.apiresponse.ApiResponse;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiRequest {

    @GET("v2/everything/")
    Call<ApiResponse> getMovieArticles(
            @Query("q") String query,
            @Query("apikey") String apiKey
    );
}

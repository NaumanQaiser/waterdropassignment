package com.one.waterdropchallenge;

import com.one.waterdropchallenge.views.HomeFragment;
import com.one.waterdropchallenge.views.MainActivity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;

import static com.one.waterdropchallenge.HomeFragmentTest.startFragment;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class)
public class MainActivityTest {

    @Test
    public void checkActivityNotNull(){
        ActivityController<MainActivity> activity = Robolectric.buildActivity(MainActivity.class);
        assertNotNull(activity);
    }

    @Test
    public void shouldHaveCorrectAppName(){
        MainActivity activity = Robolectric.setupActivity(MainActivity.class);
        String testString = activity.getResources().getString(R.string.app_name);
        assertEquals(testString, "WaterdropChallenge");
    }
    @Test
    public void HomeFragmentShouldNotBeNull(){
        HomeFragment homeFragment = new HomeFragment();
        startFragment(homeFragment);
        assertNotNull(homeFragment);

    }
}

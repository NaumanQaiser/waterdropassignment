package com.one.waterdropchallenge;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.one.waterdropchallenge.adapters.NewsArticlesAdapter;
import com.one.waterdropchallenge.models.NewsArticle;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricTestRunner.class)
public class NewsArticleAdapterTest implements NewsArticlesAdapter.OnRecyclerViewItemClickListener {

    private Context context;

    @Before
    public void setup() {
        context = RuntimeEnvironment.application;
    }

    @Test
    public void postsAdapterViewRecyclingCaption() {
        // Set up input
        List<NewsArticle> posts = Arrays.asList(
                new NewsArticle("Test News", "Nauman", "test", "20/01/2021", "This is a news article"),
                new NewsArticle("Test News2", "Nauman", "test", "20/01/2021", "This is a news article")
        );


        NewsArticlesAdapter adapter = new NewsArticlesAdapter(context, this::onClick);
        adapter.setResults(posts);
        adapter.notifyDataSetChanged();

        RecyclerView rvParent = new RecyclerView(context);
        rvParent.setLayoutManager(new LinearLayoutManager(context));

        // Run test
        NewsArticlesAdapter.ViewHolder viewHolder =
                adapter.onCreateViewHolder(rvParent, 0);

        adapter.onBindViewHolder(viewHolder, 0);
        assertEquals("Test News", viewHolder.tvTitle.getText().toString());

        adapter.onBindViewHolder(viewHolder, 1);
        assertEquals("Test News2", viewHolder.tvTitle.getText().toString());
    }

    @Override
    public void onClick(String title, String des, String imgUrl) {

    }

}

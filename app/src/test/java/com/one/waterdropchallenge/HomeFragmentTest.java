package com.one.waterdropchallenge;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.one.waterdropchallenge.views.DetailsFragment;
import com.one.waterdropchallenge.views.HomeFragment;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class)
public class HomeFragmentTest {
    HomeFragment fragment;
    //TODO
    @Before
    public void setUp() {
        fragment = new HomeFragment();
        startFragment( fragment );
    }

    public static void startFragment( Fragment fragment )
    {
        FragmentActivity activity = Robolectric.buildActivity( FragmentActivity.class )
                .create()
                .start()
                .resume()
                .get();

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add( fragment, null );
        fragmentTransaction.commit();
    }

    @Test
    public void shouldNotBeNull() {
        HomeFragment fragment = HomeFragment.getInstance();
        startFragment( fragment );
        assertNotNull( fragment );
    }
    @Test
    public void DetailsFragmentShouldNotBeNull(){
        DetailsFragment detailsFragment = new DetailsFragment();
        startFragment(detailsFragment);
        assertNotNull(detailsFragment);

    }
    //Todo Incomplete Test
    //Spinner Tests
}

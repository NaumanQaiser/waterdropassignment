package com.one.waterdropchallenge;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.one.waterdropchallenge.views.DetailsFragment;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class)
public class DetailsFragmentTest {
  DetailsFragment fragment;
//Todo
@Before
public void setUp() {
    fragment = DetailsFragment.getInstance();
    startFragment( fragment );
}

    public static void startFragment( Fragment fragment )
    {
        FragmentActivity activity = Robolectric.buildActivity( FragmentActivity.class )
                .create()
                .start()
                .resume()
                .get();

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add( fragment, null );
        fragmentTransaction.commit();
    }

    @Test
    public void shouldNotBeNull() {
        DetailsFragment fragment = DetailsFragment.getInstance();
        startFragment( fragment );
        assertNotNull( fragment );
    }

    //Todo Incomplete Test
    // DataBase Funtion Tests
}
